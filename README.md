## Created By##

Yeseom Choe

---

## Open project 

1. Click **Source** on the left side.
2. Choose **master** on the branches  
3. Download **Project1**
4. Double click **index.html**

---

## About the Project

This project is a web page aimed at changing photos by clicking two buttons. 
1.When user clicks the first button, the "dog" picture changes to the "carrot" picture. 
2.When user clicks the second button, the "carrot" picture changes to the "dog" picture.
This project complies with Accessibility for Ontarians with Disabilities Act (AODA). 


This source code from a previous assignment. 

---

## Built With

CSS
HTML
JQuery

---

## License 

I have chosen the MIT license and I am not responsible for all the problems caused by this file.

---
